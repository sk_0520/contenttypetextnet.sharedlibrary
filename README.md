# ContentTypeTextNet.SharedLibrary

ContentTypeTextNet.SharedLibrary は .NET Framework での処理を簡略化したりラッパーとして扱ったりするライブラリです。
アセンブリ名は **ContentTypeTextNet.SharedLibrary** です。

 * [Issues](https://bitbucket.org/sk_0520/contenttypetextnet.sharedlibrary/issues?status=new&status=open)を参照。
 * [![myget](https://www.myget.org/BuildSource/Badge/content-type-text-net?identifier=c33523fa-5907-4df7-a8e7-2515a1f9a320)](https://www.myget.org/gallery/content-type-text-net)

## 参照

 * [Pe](https://bitbucket.org/sk_0520/pe)
 * [MnMn](https://bitbucket.org/sk_0520/mnmn)

